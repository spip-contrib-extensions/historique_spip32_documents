<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'historique_spip32_documents_description' => 'Plugin de compatibilité : rétablir les modèles de documents de la version 3.2 de SPIP',
	'historique_spip32_documents_nom' => 'Modèles de documents SPIP 3.2',
	'historique_spip32_documents_slogan' => '',
);
