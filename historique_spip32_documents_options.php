<?php
/**
 * Options au chargement du plugin Modèles de documents 3.2
 *
 * @plugin     Modèles de documents 3.2
 * @copyright  2021
 * @author     spip team
 * @licence    GNU/GPL
 * @package    SPIP\Historique_spip32_documents\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
// Voir https://git.spip.net/spip/medias/commit/af1ba6cce5a5c8760e682e8b7fc872d076a87729 
//   et https://git.spip.net/spip/medias/commit/32dbb4ad15d31680f604254c40bc8d9772df4f1c
//   et https://git.spip.net/spip/medias/commit/9a22cde7fc8b7330b1e3bb9fa40eb04177078365

// rétablir l’utilisation des modèles img, doc et emb
if (!defined('_COMPORTEMENT_HISTORIQUE_IMG_DOC_EMB')) {
	define ('_COMPORTEMENT_HISTORIQUE_IMG_DOC_EMB', 1);
}

// conserver le fonctionnement historique (distinction portfolio et images)
if (!defined('_COMPORTEMENT_HISTORIQUE_PORTFOLIO')) {
	define ('_COMPORTEMENT_HISTORIQUE_PORTFOLIO', 1);
}

function historique_spip32_documents_insert_head_css($flux){
	$flux .= "\r\n".'<link rel="stylesheet" href="'.find_in_path('css/historique_spip32_documents.css').'" type="text/css" media="all" />'."\r\n";
	return $flux;
}
